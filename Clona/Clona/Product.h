#pragma once
#include <cstdint>
#include <string>

class Product
{
public:
	Product(uint16_t id, const std::string & name, float pret, uint8_t vat, const std::string & dateOrType);
	bool isPerisable();
	float calculatePrice();
private:
	uint16_t m_id;
	std::string m_name;
	float m_pret;
	uint8_t m_vat;
	std::string m_dateOrType;
};


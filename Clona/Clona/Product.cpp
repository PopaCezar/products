#include "Product.h"

Product::Product(uint16_t id, const std::string & name, float pret, uint8_t vat, const std::string & dateOrType) :
	m_id(id), m_name(name), m_pret(pret), m_vat(vat), m_dateOrType(dateOrType) {}

bool Product::isPerisable()
{
	return false;
}

float Product::calculatePrice()
{
	return m_pret + m_pret * m_vat;
}



